package com.xsis.abstraction;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.validation.constraints.Size;

@MappedSuperclass
public abstract class SuperUser {
	@Column(name="created_on", nullable=true)
	protected LocalDateTime createdOn;
	
	@Column(name="created_by", nullable=true)
	@Size(max=25)
	protected String createdBy;
	
	@Column(name="modified_on", nullable=true)
	protected LocalDateTime modifiedOn;
	
	
	@Column(name="modified_by", nullable=true)
	@Size(max=25)
	protected String modifiedBy;
	
	@Column(name="is_delete", nullable=true)
	protected boolean isDelete=false;
	
	@PrePersist
	void createdOn() {
		this.createdOn= LocalDateTime.now();
		this.createdBy= "xsis";
	}
}
