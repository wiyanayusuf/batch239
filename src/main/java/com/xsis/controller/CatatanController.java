package com.xsis.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xsis.model.Catatan;
import com.xsis.service.CatatanService;

@Controller
@RequestMapping(value="/catatan")
public class CatatanController {
	
	@Autowired
	private CatatanService catatanService;
	
	@GetMapping("/")
	public String catatan(Model model) {
		model.addAttribute("catatan", catatanService.findCatatan());
		return "catatan/catatan";
		
	}
	
	/*@GetMapping("new")
	public String newCate(Model model) {
		model.addAttribute("cat", new Catatan());
		return "catatan/add";
	}*/
	
	@GetMapping("edit/{id}")
	public String editCat(@PathVariable(name = "id") Long id, Model model) {
		
		model.addAttribute("cat", catatanService.findCatatanById(id));
		return "catatan/edit";
	}
	
	@PostMapping("save")
	public String saveCat(@Valid Catatan cat,Errors errors) {
		catatanService.save(cat);
		return "redirect:/catatan/";
	}	
	
	@GetMapping("delete/{id}")
	public String deleteCate(@PathVariable(name = "id") Long id) {
		catatanService.deleteCatatanById(id);
		
		return "redirect:/catatan/";
	}
}
