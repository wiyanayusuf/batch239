package com.xsis.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="catatan")
public class Catatan {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="judulCatatan")
	private String judulCatatan;
	
	@Column(name="jenisCatatan")
	private String jenisCatatan;
	
	@Column(name="deskripsiCatatan")
	private String deskripsiCatatan;
	
	
	public Catatan(String judulCatatan, String jenisCatatan, String deskripsiCatatan) {
		super();
		this.judulCatatan = judulCatatan;
		this.jenisCatatan = jenisCatatan;
		this.deskripsiCatatan = deskripsiCatatan;
	}
	
	public String getJudulCatatan() {
		return judulCatatan;
	}
	
	public void setJudulCatatan(String judulCatatan) {
		this.judulCatatan = judulCatatan;
	}
	
	public String getJenisCatatan() {
		return jenisCatatan;
	}
	
	public void setJenisCatatan(String jenisCatatan) {
		this.jenisCatatan = jenisCatatan;
	}
	
	public String getDeskripsiCatatan() {
		return deskripsiCatatan;
	}
	
	public void setDeskripsiCatatan(String deskripsiCatatan) {
		this.deskripsiCatatan = deskripsiCatatan;
	}

	
	
	
}
