package com.xsis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch239Application {

	public static void main(String[] args) {
		SpringApplication.run(Batch239Application.class, args);
	}

}
