package com.xsis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.xsis.model.Catatan;
import com.xsis.repository.CatatanRepository;

public class CatatanServiceImpl implements CatatanService {

	
	@Autowired
	CatatanRepository catRepo;

	@Override
	public List<Catatan> findCatatan() {
		// TODO Auto-generated method stub
		return catRepo.findAll();
	}

	@Override
	public Catatan save(Catatan catatan) {
		// TODO Auto-generated method stub
		return catRepo.save(catatan);
	}

	@Override
	public Optional<Catatan> findCatatanById(Long id) {
		// TODO Auto-generated method stub
		return catRepo.findById(id);
	}

	@Override
	public void deleteCatatanById(Long id) {
		// TODO Auto-generated method stub
		catRepo.deleteById(id);
	}
	
	
}
