package com.xsis.service;

import java.util.List;
import java.util.Optional;

import com.xsis.model.Catatan;

public interface CatatanService {
	List<Catatan> findCatatan();
	
	Catatan save(Catatan catatan);
	
	Optional<Catatan> findCatatanById(Long id);
	
	void deleteCatatanById(Long id);
}
