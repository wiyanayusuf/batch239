package com.xsis.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.xsis.model.Catatan;

public interface CatatanRepository extends JpaRepository<Catatan,Long>{

}
